from project.process.import_data.import_data import test_data_read
from project.process.text_preprocess_and_entities.text_preprocess import single_text_preprocess_and_entities
from project.process.create_asum_files.create_files import create_asum_files
from project.process.run_asum.run_asum import run_asum
from project.process.create_useful_collection.create_useful_collections import create_collections_for_dashboard
from multiprocessing import Pool

# Parallelize the second execution of the flow
if __name__ == '__main__':
    '''
    # Import Data (1)
    print("Import Data")
    articles = test_data_read()

    # Execute text-preprocess: (2)
    # 1) scraping
    # 2) text cleaner and date normalization 
    # 3) sentence and word tokenizing
    # 4) clean sentences and words for asum
    print("Text Preprocessing and Find Entities (Parallelized)")
    urls_and_dates = [ (url, date) for url, date in articles]
    with Pool() as p:
        p.map(single_text_preprocess_and_entities, urls_and_dates)
    
    # Create ASUM files (3)
    # 1) create files for ASUM
    # 2) save results in MongoDB
    print("Create ASUM files")
    create_asum_files()
    
    # save data for the first and teh second tab (4)
    print("Create Collections for the final Dashboard")
    create_collections_for_dashboard()
    
    # Run java ASUM (5)
    print("Run ASUM")
    run_asum()
    
    print("EXECUTION FINISHED")
    '''