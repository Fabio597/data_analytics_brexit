FROM openjdk:11

ENV PYTHONUNBUFFERED 1

# Install python3.7 & pip
RUN apt-get update && apt-get install python3
RUN apt-get install -y python3-pip
RUN apt-get clean

# Install spacy and download en_core_web_sm
RUN pip3 install -U pip setuptools wheel && \
    pip3 install -U spacy && \
    python3 -m spacy download en_core_web_sm;

# create root directory for my project in the container
RUN mkdir /brexit_project

# Set the working directory
WORKDIR /brexit_project

# Copy the current directory contents into the container
# Add projects folder
ADD project /brexit_project/project
# Add requirements.txt file
ADD requirements.txt /brexit_project/
# Add main.py file
ADD main.py /brexit_project/

# Install any needed packages specified in requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt