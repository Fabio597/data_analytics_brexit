import pymongo
import datetime

# connect to mongo db with pymongo
def get_db():
  # Manually
  #client = pymongo.MongoClient("mongodb://localhost:27017/")
  # Docker
  client = pymongo.MongoClient("mongodb://mongo:27017/")
  db = client["brexitdb"]
  return db

def get_collection(collection = 'ARTICLES'):
  db = get_db()
  if collection == 'ARTICLES':
    return db["articles"]
  elif collection == 'WORDSLIST':
    return db["wordslist"]
  elif collection == 'WORDSSTEMMING':
    return db["wordsstemming"]
  elif collection == 'MEDIAENTITIES':
    return db['media_entities']
  elif collection == 'TIMELINEENTITIES':
    return db['time_line_entities']

articles = get_collection(collection = 'ARTICLES')
wordslist = get_collection(collection = 'WORDSLIST')
wordsstemming = get_collection(collection = 'WORDSSTEMMING')
media_entities = get_collection(collection = 'MEDIAENTITIES')
time_line_entities = get_collection(collection = 'TIMELINEENTITIES')

# RETRIEVE FUCTIONS

# number of articles
def get_number_of_articles():
  return articles.count()

# get all media (distinct)
def get_distinct_media():
  return media_entities.find({}).distinct("media")

# get all media articles
def get_media_articles(media):
  return list(articles.find({"author": media}, {"url": 1, "text": 1}))

def get_article_by_url(url):
  return articles.find_one({"url": url}, {"text" :1, "ner": 1, "nel": 1})

# get all dates (distinct)
def get_distinct_dates():
  return time_line_entities.find({}).distinct("date")

# get entitis when selecting media
def get_entities_by_media(media):
  return media_entities.find({"media": media}, {"entities": 1, "entities_list": 1, "_id": 0})

# db.time_line_entities.find({"date": {$gte: new ISODate("from"), $lte: new ISODate("to")}}).count()
def get_entities_between_dates(from_date, to_date):
  return list(time_line_entities.find({"date": {'$gte': from_date, '$lte': to_date}}, {"entities": 1, "entities_list": 1, "_id": 0}))

# db.articles.aggregate({"$group": {_id: "$author", count: {$sum:1}}})
def get_media_count():
  return articles.aggregate([{"$group": {"_id": "$author", "count": {"$sum":1}}}, { "$sort": { "count": -1 }}])

# db.articles('articles').find({}, {"words": 1})
def get_all_words():
  return media_entities.find({}, {"entities_list": 1, "_id": 0})

def get_article_between_dates(from_date, to_date):
  return list(articles.find({"date": {'$gte': from_date, '$lte': to_date}}, {"url": 1, "_id": 0}))