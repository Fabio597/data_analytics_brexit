from operations.data_processing import from_timestamp_to_string
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

# COMPONENTS
def create_dropdown(id, elements, initial_value, placeholder = "Select", style = {}, multi=False):
    return dcc.Dropdown(
                id=id,
                options=elements,
                value=initial_value,
                placeholder=placeholder,
                style = style,
                multi = multi
            )

def create_radio_items_dash(id, elements, initial_value = '', labelStyle = {}):
    return dcc.RadioItems(
                id=id,
                options=elements,
                value= initial_value,
                labelStyle= labelStyle,
            )

def create_radio_items_bootstrap(id, elements, initial_value = '', labelClassName = 'date-group-labels', labelCheckedClassName = 'date-group-labels-checked', className = 'date-group-items', inline = True):
    return dbc.RadioItems(
                id=id,
                options=elements,
                value=initial_value,
                labelClassName=labelClassName,
                labelCheckedClassName=labelCheckedClassName,
                className=className,
                inline=True,
            )

def create_range_slider(id, min, max, style_min = {}, style_max = {}):
    return dcc.RangeSlider(
            id = id,
            min= min,
            max= max,
            value=[min, max],
            marks={
                min: {'label': from_timestamp_to_string(min), 'style': style_min},
                max: {'label': from_timestamp_to_string(max), 'style': style_max}
            }
        )

def create_wordcloud_image(id, wordcloud, style = {}):
    return html.Img(
                id=id,
                src=wordcloud, 
                style=style
            )

def create_input_text(id, initial_value, placeholder = "", style = {}):
    return dcc.Input(
                id=id,
                placeholder=placeholder,
                type='text',
                value=initial_value,
                style = style
            )

def create_div(id, children = [], style = {}):
    return html.Div(id = id, children = children, style = style)

def create_h1(text = '', style = {}):
    return html.H1(children = text, style = style)

def create_h3(text = '', style = {}):
    return html.H3(children = text, style = style)

def create_p(text, style = {}):
    return html.P(text, style = style)

def create_tabs(id, tabs_list):
    return dcc.Tabs(id=id, value='tab-1-example', children=[dcc.Tab(label=tab[0], value=tab[1]) for tab in tabs_list])

def create_table(table_header, table_body):
    table = dbc.Table(table_header + table_body, bordered=True)
    return table

def create_table_header(header_list):
    table_header = [
        html.Thead(html.Tr([html.Th(head) for head in header_list]))
    ]
    return table_header

def create_table_body_for_entities(text, image):
    row = html.Tr([
        html.Td(dcc.Markdown(text)) if text is not None else "No Testo",
        html.Td(html.Img(src=image)) if image is not None else "No Image"
    ])
    table_body = [html.Tbody([row])]
    return table_body

def create_table_body(rows):
    return [html.Tbody(rows)]

def create_histogram_chart(id, data, pagination, title):
    return dcc.Graph(
                id=id,
                figure={
                    'data': data[10*(int(pagination) - 1):10*int(pagination)],
                    'layout': {
                        'title': title
                    }
                }
            )

def create_card(title, description):
    return dbc.Card(
    dbc.CardBody(
        [
            html.H5(title, className="card-title"),
            html.P(description),
        ]
    )
)

def create_card_row(cards):
    return dbc.Row(cards)

def create_card_col(card, width):
    return dbc.Col(card, width = width)