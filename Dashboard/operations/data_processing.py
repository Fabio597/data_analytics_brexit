from operations.database import get_distinct_media, get_entities_by_media, get_distinct_dates, get_entities_between_dates, get_media_count, get_media_articles, get_number_of_articles, get_all_words, get_article_by_url, get_article_between_dates
from operations.regex_design import apply_cleaning, clean_prob_word
from wordcloud import WordCloud
from collections import Counter
from io import BytesIO
import pandas as pd
import datetime
import base64
import nltk
nltk.download('punkt')
import os

def get_list_of_media():
    all_media = get_distinct_media()
    list_of_media = [{"label": apply_cleaning(media), "value": media} for media in all_media]
    if len(list_of_media) > 0:
        return (list_of_media, all_media[0])
    else:
        return(["--seleziona--"],"")

def get_number_of_articles_analyzed():
    return get_number_of_articles()

def get_media_entities(media, search_entity = None):
    result = list(get_entities_by_media(media))[0]
    entities_dict = {}
    if search_entity is not None:
        for key in result['entities'].keys():
            if search_entity in key:
                entities_dict[key] = result['entities'][key]
    else:
        entities_dict = result['entities']
    entities_list = result['entities_list']
    return (entities_dict, entities_list)

def get_media_entity_info(media, entity):
    (entities_info, _) = get_media_entities(media)
    return entities_info[entity]

def read_csv():
    file = open(os.path.join(os.path.dirname(__file__), "../ASUM_files_output/STO2-T25-S2(2)-A0.1-B0.001,0.2,0.2-G1.0,1.0-I1000-ProbWords.csv"))
    asum_data = pd.read_csv(file)
    return asum_data

def read_columns():
    table = read_csv()
    data = [{"label": column, "value": column} for column in table.columns]
    return data

def get_asum_words(column, k, prob, selector):
    table = read_csv()
    col = list(table[column])

    if 'NUMB' in selector:
        col = col[0:int(k)]

    if 'PROB' in selector:
        prob_words = []
        for value in col:
            if float(clean_prob_word_before_show(value).split(" ")[1]) >= float(prob):
                prob_words.append(value)
        col = prob_words

    return col

def get_articles_by_media(media):
    return get_media_articles(media)

def get_articles_info(url):
    info = get_article_by_url(url)
    ner = info["ner"]
    nel = info["nel"]
    text = info["text"]
    entities_dict = {}
    for entity in ner:
        if entity["word"].lower() not in entities_dict:
            entities_dict[entity["word"].lower()] = entity["sentiment"]
        else:
            entities_dict[entity["word"].lower()] += entity["sentiment"]
    for entity in nel:
        if entity["word"].lower() not in entities_dict:
            entities_dict[entity["word"].lower()] = 0
    
    return text, entities_dict

def clean_prob_word_before_show(word):
    return clean_prob_word(word)

def get_min_max_dates():
    dates = get_distinct_dates()
    timestamps = [datetime.datetime.timestamp(date) for date in dates]
    min_timestamp = min(timestamps)
    max_timestamp = max(timestamps)
    return(int(min_timestamp), int(max_timestamp))

def from_timestamp_to_string(timestamp):
    date = datetime.datetime.fromtimestamp(timestamp)
    return date.strftime("%d %B, %Y")

# 2016-12-13 00:00:00.000Z
def get_entities_from_range(from_date, to_date, search_entity = None):
    from_date = datetime.datetime.fromtimestamp(from_date)
    to_date = datetime.datetime.fromtimestamp(to_date)
    result = get_entities_between_dates(from_date, to_date)
    entity_set = set()
    entity_list = []
    for ents in result:
        entity_set = entity_set.union(ents['entities'].keys())
        entity_list = entity_list + ents['entities_list']
    if search_entity is not None:
        entity_set = set(filter(lambda n: search_entity in n, entity_set))
    return (entity_set, entity_list)

def get_resources_for_timeline(range, entity):
    from_date = datetime.datetime.fromtimestamp(range[0])
    to_date = datetime.datetime.fromtimestamp(range[1]) 
    result = get_entities_between_dates(from_date, to_date)
    for ents in result:
        if entity in ents['entities']:
            return ents['entities'][entity]
    return None

def get_number_of_article_per_media():
    data = []
    counter = 1
    for media in list(get_media_count()):
        data.append(
            {'x': [counter], 'y': [media['count']], 'type': 'bar', 'name': apply_cleaning(media['_id'])} 
        )
        counter += 1
    return data

# create wordcloud
def create_wordcloud(entities):
    img = BytesIO()
    wordcloud = WordCloud(background_color='white', width=480, height=360)
    wordcloud.generate_from_frequencies(Counter(entities))
    wordcloud.to_image().save(img, format='PNG')
    return 'data:image/png;base64,{}'.format(base64.b64encode(img.getvalue()).decode())

def find_sentences(text, word):
    sentences = nltk.tokenize.sent_tokenize(text)
    sentences_to_show = []
    for sentence in sentences:
        if word.lower() in nltk.word_tokenize(sentence.lower()):
            sentences_to_show.append(sentence)
    return sentences_to_show

def get_words_number():
    words = get_all_words()
    final_list_of_words = []
    for list_of_words in words:
        final_list_of_words = final_list_of_words + list_of_words['entities_list']
    return len(final_list_of_words)

def get_words():
    words = get_all_words()
    final_list_of_words = []
    for list_of_words in words:
        final_list_of_words = final_list_of_words + list_of_words['entities_list']
    words_counter = Counter(final_list_of_words).most_common()
    data = []
    counter = 1
    for word, value in words_counter:
        data.append(
            {'x': [counter], 'y': [value], 'type': 'bar', 'name': word.lower()}
        )
        counter += 1
    return data

def get_articles_from_to(from_date, to_date):
    from_date = datetime.datetime.fromtimestamp(from_date)
    to_date = datetime.datetime.fromtimestamp(to_date)
    result = get_article_between_dates(from_date, to_date)
    return result