import re

def apply_cleaning(word):
    new_word = clean_www(word)
    new_word = clean_com(new_word)
    new_word = clean_dot(new_word)
    return new_word

def clean_www(word):
    return re.sub("www", "", word)

def clean_dot(word):
    return re.sub("\.", " ", word)

def clean_com(word):
    return re.sub("com", "", word)

def clean_prob_word(word):
    return re.sub("\(|\)", "", word)