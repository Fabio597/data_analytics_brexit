# -*- coding: utf-8 -*-
# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

# Import libraries
import dash
import dash_bootstrap_components as dbc
from dash import html
from dash.dependencies import Input, Output, State
from operations.data_processing import get_list_of_media, get_media_entities, read_columns, get_asum_words, get_media_entity_info, get_min_max_dates, from_timestamp_to_string, get_entities_from_range, get_resources_for_timeline, get_number_of_article_per_media, clean_prob_word_before_show, create_wordcloud, get_articles_by_media, get_number_of_articles_analyzed, find_sentences, get_words, get_words_number, get_articles_info, get_articles_from_to
from operations.components import create_tabs, create_div, create_h1, create_h3, create_dropdown, create_wordcloud_image, create_radio_items_dash, create_table, create_table_header, create_table_body_for_entities, create_radio_items_bootstrap, create_input_text, create_table_body, create_range_slider, create_p, create_histogram_chart, create_card, create_card_row, create_card_col

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', dbc.themes.BOOTSTRAP]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Constants
SENTIMENT_RADIO_ITEMS = [{'label': 'All', 'value': 'ALL'}, {'label': 'Positive', 'value': 'POS'}, {'label': 'Negative', 'value': 'NEG'}]
PROB_NUMB_ITEMS = [{'label': 'Prob', 'value': 'PROB'}, {'label': 'Number', 'value': 'NUMB'}]
SENTIMENT_DEFAULT_VALUE = 'ALL'

# Layout of dash
app.layout = dbc.Container([
    create_div(id= 'main_container', children=[
        create_h1(text = 'Brexit Analysis', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'green', 'font-weight': 'bold'}),
        create_tabs(id = "tabs", tabs_list=[('Entities by Media', 'tab-media-article'), ('Topic - Sentiment', 'tab-topic-sentiment'), ('Timeline', 'tab-time-line'), ('Media Articles', 'articles-tab'), ('Timeline Articles', 'articles-tab-timeline'), ('Statistics', 'statistics-tab')]),
        create_div(id = 'tabs-content')
    ]),
])


# FUNCTIONS ----------------------------------------------------------------------------
# First Tab ****************************************************************************
def tab_media_articles():
    list_of_media = get_list_of_media()
    return create_div(id = 'first-tab-div', children= [
        create_h3(text = 'Analyzed Media:', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_dropdown(id = "media-dropdown-id", elements = list_of_media[0], initial_value = list_of_media[1], style = {'margin-left': 'auto', 'margin-right': 'auto', 'width': '70%'}),
        create_h3(text = 'Treated Topics', style={'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_div(id = 'media-dropdown-selected')
    ])

# show entities found in that media
def show_entities(entities_list):
    return create_div(id = '', children = [
        create_wordcloud_image(id = 'image_wc', wordcloud = create_wordcloud(entities_list), style = {'display': 'block', 'margin-left': 'auto', 'margin-right': 'auto', 'width': '50%'}),
        create_radio_items_dash(id = 'entities-filter', elements = SENTIMENT_RADIO_ITEMS, initial_value = SENTIMENT_DEFAULT_VALUE, labelStyle = {'display': 'inline-block', 'margin-left': '10px'}),
        create_input_text(id = 'search-entity-first', initial_value = '', placeholder = 'search-entity', style = {'margin-left': 'auto', 'margin-right': 'auto', 'width': '20%'}),
        create_div(id = 'entities-div'),
        create_div(id = 'entity-info-first')
    ])

# show table with entity info:  text, image and concept
def show_entity_info(media, entity):
    entity_info = get_media_entity_info(media, entity)
    text = entity_info['text'] if 'text' in entity_info else None
    image = entity_info['image'] if 'image' in entity_info else None

    if text is not None or image is not None:
        return create_table(create_table_header(['text', 'image']), create_table_body_for_entities(text, image))
    else:
        return create_h3(text = "No Info", style= {'text-align': 'center'})
    
# callbacks first tab
# dropdown callback
@app.callback(
    dash.dependencies.Output('media-dropdown-selected', 'children'),
    [dash.dependencies.Input('media-dropdown-id', 'value')])
def update_output(value):
    (_, entities_list) = get_media_entities(value, None)
    return show_entities(entities_list)

# radio button callback for first tab
@app.callback(Output("entity-info-first", "children"), [Input("radios-first", "value"), Input('media-dropdown-id', 'value')])
def display_value(value, media):
    return show_entity_info(media, value)

# radio item sentiment callback
@app.callback(
    dash.dependencies.Output('entities-div', 'children'),
    [dash.dependencies.Input('media-dropdown-id', 'value'), dash.dependencies.Input('entities-filter', 'value'), dash.dependencies.Input('search-entity-first', 'value')])
def filter_entities_first(value, entities_sentiment, search_entity):
    entities_dict = {}
    if search_entity:
        (entities_dict, _) = get_media_entities(value, search_entity)
    else:
        (entities_dict, _) = get_media_entities(value, None)
    if entities_sentiment == 'POS':
        data = []
        for e in entities_dict:
            if entities_dict[e]["sentiment"] > 0:
                data.append(e)
        return create_radio_items_bootstrap(id = 'radios-first', elements = [ {'label': entity, 'value': entity} for entity in data])
    if entities_sentiment == 'NEG':
        data = []
        for e in entities_dict:
            if entities_dict[e]["sentiment"] < 0:
                data.append(e)
        return create_radio_items_bootstrap(id = 'radios-first', elements = [ {'label': entity, 'value': entity} for entity in data])
    return create_radio_items_bootstrap(id = 'radios-first', elements = [ {'label': entity, 'value': entity} for entity in entities_dict])

# End First Tab ****************************************************************************
# Second Tab ****************************************************************************
def tab_topic_sentiment():
    data = read_columns()
    return  create_div(id = 'second_tab_div', children= [
        create_h3(text = 'Topics', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_div(id = 'inputs-div', children = [
            create_dropdown(id = 'topic-dropdown', elements = data, initial_value = 'S0-T0'),
            create_dropdown(id = 'numb-prob', elements = PROB_NUMB_ITEMS, initial_value = 'NUMB', multi=True),
            create_input_text(id = 'k-value', initial_value = '20', placeholder = 'K value'),
            create_input_text(id = 'min-prob-value', initial_value = '0.010', placeholder = 'Min Prob. value')
        ]),
        create_div(id = 'topic-div')
    ])

def show_prob_words(word_list):
    rows = []
    for value in word_list:
        current_string = clean_prob_word_before_show(value)
        splitted_string = current_string.split(" ")
        rows.append(html.Tr([html.Td(splitted_string[0]), html.Td(splitted_string[1])]))

    return create_table(create_table_header(['Word', 'Probabilities']), create_table_body(rows))

# callbacks second tab
# table filters callback
@app.callback(
    dash.dependencies.Output('topic-div', 'children'),
    [dash.dependencies.Input('topic-dropdown', 'value'),
    dash.dependencies.Input('k-value', 'value'),
    dash.dependencies.Input('min-prob-value', 'value'),
    dash.dependencies.Input('numb-prob', 'value')])
def update_topic(value, k, prob, selector):
    return show_prob_words(get_asum_words(value, k, prob, selector))

# End Second Tab ****************************************************************************
# Third Tab ****************************************************************************
def tab_time_line():
    (min, max) = get_min_max_dates()
    return create_div(id = 'slider-div', children= [
        create_range_slider(id = 'my-range-slider', min = min, max = max, style_min = {'color': '#77b0b1'}, style_max = {'color': '#77b0b1'}),
        create_div(id='output-container-range-slider')
    ], style = {'width': '80%', 'margin-left': 'auto', 'margin-right': 'auto', 'margin-top': '30px'})

def show_time_line_results(value):
    timestamp_from_value = value[0]
    timestamp_to_value = value[1]
    from_date = from_timestamp_to_string(timestamp_from_value)
    to_date = from_timestamp_to_string(timestamp_to_value)
    (_, entity_list) = get_entities_from_range(timestamp_from_value, timestamp_to_value)
    return create_div(id = 'div-timeline-results', children = [
        create_p(text = 'Range Selected: '+ from_date + ' - ' + to_date + '', style = {'margin-left': 'auto', 'margin-right': 'auto', 'width': '50%', 'font-weight': 'bold', 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_wordcloud_image(id='image_wc_third', wordcloud = create_wordcloud(entity_list), style = {'display': 'block', 'margin-left': 'auto', 'margin-right': 'auto', 'width': '50%'}),
        create_radio_items_dash(id = 'entities-filter-second', elements = SENTIMENT_RADIO_ITEMS, initial_value = SENTIMENT_DEFAULT_VALUE, labelStyle = {'display': 'inline-block', 'margin-left': '10px'}),
        create_input_text(id = 'search-entity-second', initial_value = '', placeholder = 'search-entity', style = {'margin-left': 'auto', 'margin-right': 'auto', 'width': '20%'}),
        create_div(id ='entities-div-second'),
        create_div(id ='entity-info-third')
    ])

def show_entity_info_time_line(resources):
    text = resources['text'] if 'text' in resources else None
    image = resources['image'] if 'image' in resources else None

    if text is not None or image is not None:
        return create_table(create_table_header(['Text', 'Image']), create_table_body_for_entities(text, image))
    else:
        return create_h3(text = "No Info", style= {'text-align': 'center'})

# callback for time line
@app.callback(
    dash.dependencies.Output('output-container-range-slider', 'children'),
    [dash.dependencies.Input('my-range-slider', 'value')])
def update_output(value):
    return show_time_line_results(value)

# radio button callback for third tab
@app.callback(Output("entity-info-third", "children"), [dash.dependencies.Input('my-range-slider', 'value'), Input("radios-third", "value")])
def display_value_third(range, entity):
    resources = get_resources_for_timeline(range, entity)
    return show_entity_info_time_line(resources)

# radio item sentiment callback
@app.callback(
    dash.dependencies.Output('entities-div-second', 'children'),
    [dash.dependencies.Input('my-range-slider', 'value'), dash.dependencies.Input('entities-filter-second', 'value'), dash.dependencies.Input('search-entity-second', 'value')])
def filter_entities_second(value, entities_sentiment, search_entity):
    timestamp_from_value = value[0]
    timestamp_to_value = value[1]
    entity_set = set()
    if search_entity:
        (entity_set, _) = get_entities_from_range(timestamp_from_value, timestamp_to_value, search_entity)
    else:
        (entity_set, _) = get_entities_from_range(timestamp_from_value, timestamp_to_value)
    if entities_sentiment == 'POS':
        data = []
        for entity in entity_set:
            if get_resources_for_timeline(value, entity)['sentiment'] > 0:
                data.append(entity)
        return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")

    if entities_sentiment == 'NEG':
        data = []
        for entity in entity_set:
            if get_resources_for_timeline(value, entity)['sentiment'] < 0:
                data.append(entity)
        return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")
    return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in entity_set], labelClassName="date-group-labels_v3")
# End Third Tab ****************************************************************************
# Fourth Tab ****************************************************************************
def tab_statistics():
    words_number = get_words_number()
    number_of_articles_analyzed = get_number_of_articles_analyzed()
    list_of_media = get_list_of_media()
    data = get_number_of_article_per_media()
    n = int(len(data)/10) + 1
    wn = int(words_number/10) + 1
    card_1 = create_card("Number of media analyzed", len(list_of_media[0]))
    card_2 = create_card("Total number of article analyzed", number_of_articles_analyzed)
    return create_div(id = 'fourth-tab-div', children = [
        create_h3(text = 'Statistics', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_card_row([create_card_col(card_1, width = 6), create_card_col(card_2, width = 6)]),
        create_dropdown(id='pagination_dropdown', elements = [{"label": index, "value": index} for index in range(1, n + 1)], initial_value = '1', placeholder = 'Select pagination to display media', style = {'margin-top': '15px'}),
        create_div(id='graph-media-output'),
        create_dropdown(id='pagination_dropdown_words', elements = [{"label": index, "value": index} for index in range(1, wn + 1)], initial_value = '1', placeholder = 'Select pagination to display words', style = {'margin-top': '15px'}),
        create_div(id = 'graph-words-output')
    ])

# callback fourth tab
@app.callback(
    dash.dependencies.Output('graph-media-output', 'children'),
    [dash.dependencies.Input('pagination_dropdown', 'value')])
def update_graph_media(pagination):
    data = get_number_of_article_per_media()
    return create_histogram_chart(id='histogram-chart', data = data, pagination = pagination, title = 'Media Distribution')

@app.callback(
    dash.dependencies.Output('graph-words-output', 'children'),
    [dash.dependencies.Input('pagination_dropdown_words', 'value')])
def update_graph_words(pagination):
    data = get_words()
    return create_histogram_chart(id='histogram-chart', data = data, pagination = pagination, title = 'Entities Distribution')
    
# End Fourth Tab ****************************************************************************
# Fifth Tab ---------------------------------------------------------------------------------
def tab_articles():
    list_of_media = get_list_of_media()
    return create_div(id = 'fourth-tab-div', children = [
        create_h3(text = 'Media Articles', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_dropdown(id = "media-dropdown-article-shaw", elements = list_of_media[0], initial_value = list_of_media[1], style = {'margin-left': 'auto', 'margin-top': '15px', 'margin-right': 'auto', 'width': '70%'}),
        create_div(id = 'articles-list', style={"margin-top": "15px"}),
        create_div(id = "text_shower", style = {'margin-top': '15px'})
    ])

@app.callback(
    dash.dependencies.Output('articles-list', 'children'),
    [dash.dependencies.Input('media-dropdown-article-shaw', 'value')])
def show_articles(value):
    articles = get_articles_by_media(value)
    list_of_articles = [{'label': article['url'], 'value': article['url']} for article in articles]
    return create_radio_items_bootstrap(id = 'articles-link-div', elements = list_of_articles, labelClassName="date-group-labels_v2")

@app.callback(
    dash.dependencies.Output('text_shower', 'children'),
    [dash.dependencies.Input('articles-link-div', 'value')])
def article_text_shower(url):
    text, _ = get_articles_info(url)
    return create_div(id="text-sentences-shower", children=[
        create_p(text),
        create_div(id="word-chooser", children=[
            create_radio_items_dash(id = 'entities-filter-articles', elements = SENTIMENT_RADIO_ITEMS, initial_value = SENTIMENT_DEFAULT_VALUE, labelStyle = {'display': 'inline-block', 'margin-left': '10px'}),
            create_div(id = "entities_single_article"),
            create_h3(text = 'search word inside text:'),
            create_input_text(id = 'word-value', initial_value = '', placeholder = 'search word'),
        ]),
        create_div(id = "sentences_table")
    ])


@app.callback(
    dash.dependencies.Output('entities_single_article', 'children'),
    [dash.dependencies.Input('articles-link-div', 'value'), dash.dependencies.Input('entities-filter-articles', 'value')])
def entities_article_single(url, entities_sentiment):
    _, entities_dict = get_articles_info(url)
    if entities_sentiment == 'POS':
        data = []
        for entity_key in entities_dict.keys():
            if entities_dict[entity_key] > 0:
                data.append(entity_key)
        return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")

    if entities_sentiment == 'NEG':
        data = []
        for entity_key in entities_dict.keys():
            if entities_dict[entity_key] < 0:
                data.append(entity_key)
        return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")
    return create_radio_items_bootstrap(id = 'radios-third', elements = [ {'label': entity, 'value': entity} for entity in entities_dict.keys()], labelClassName="date-group-labels_v3")


@app.callback(
    dash.dependencies.Output('sentences_table', 'children'),
    [dash.dependencies.Input('articles-link-div', 'value'),
    dash.dependencies.Input('word-value', 'value')])
def sentence_tokenizer_shower(url, word):
    text, _ = get_articles_info(url)
    sentences = find_sentences(text, word)
    return create_table(create_table_header(['sentences']), [html.Tbody(sentence) for sentence in sentences])
# End fifth tab
# Sixth tab -------------------------------------------
def tab_articles_timeline():
    min, max = get_min_max_dates()
    return create_div(id = 'fourth-tab-div', children = [
        create_h3(text = 'TimeLine Articles', style = {'font-family': ['Times New Roman', 'Times', 'serif'], 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_range_slider(id = 'article-slider', min = min, max = max, style_min = {'color': '#77b0b1'}, style_max = {'color': '#77b0b1'}),    
        create_div(id = "articles-slider-links")
    ])

@app.callback(
    dash.dependencies.Output('articles-slider-links', 'children'),
    [dash.dependencies.Input('article-slider', 'value')]
)
def show_links_timeline(value):
    timestamp_from_value = value[0]
    timestamp_to_value = value[1]
    from_date = from_timestamp_to_string(timestamp_from_value)
    to_date = from_timestamp_to_string(timestamp_to_value)
    articles = get_articles_from_to(timestamp_from_value, timestamp_to_value)
    list_of_articles = [{'label': article['url'], 'value': article['url']} for article in articles]
    return [
        create_p(text = 'Range Selected: '+ from_date + ' - ' + to_date + '', style = {'margin-left': 'auto', 'margin-right': 'auto', 'width': '50%', 'font-weight': 'bold', 'text-align': 'center', 'color': 'DarkOliveGreen'}),
        create_radio_items_bootstrap(id = 'articles-timeline-link-div', elements = list_of_articles, labelClassName="date-group-labels"),
        create_div("timeline-article-entities")
    ]

@app.callback(
    dash.dependencies.Output('timeline-article-entities', 'children'),
    [dash.dependencies.Input('articles-timeline-link-div', 'value')]
)
def show_timeline_article_entities(link):
    text, _ = get_articles_info(link)
    return create_div(id="text-sentences-shower-timeline", children=[
        create_p(text),
        create_div(id="word-chooser-timeline", children=[
            create_radio_items_dash(id = 'entities-filter-articles-timeline', elements = SENTIMENT_RADIO_ITEMS, initial_value = SENTIMENT_DEFAULT_VALUE, labelStyle = {'display': 'inline-block', 'margin-left': '10px'}),
            create_div(id = "entities-single-article-timeline"),
            create_div(id = 'entity-statistics'),
            create_h3(text = 'search word inside text:'),
            create_input_text(id = 'word-value-timeline', initial_value = '', placeholder = 'search word'),
        ]),
        create_div(id = "sentences-table-timeline"),
    ])

@app.callback(
    dash.dependencies.Output('sentences-table-timeline', 'children'),
    [dash.dependencies.Input('articles-timeline-link-div', 'value'),
    dash.dependencies.Input('word-value-timeline', 'value')])
def sentence_tokenizer_shower_timeline(url, word):
    text, _ = get_articles_info(url)
    sentences = find_sentences(text, word)
    return create_table(create_table_header(['sentences']), [html.Tbody(sentence) for sentence in sentences])

@app.callback(
    dash.dependencies.Output('entities-single-article-timeline', 'children'),
    [dash.dependencies.Input('articles-timeline-link-div', 'value'), dash.dependencies.Input('entities-filter-articles-timeline', 'value')])
def entities_article_single(url, entities_sentiment):
    _, entities_dict = get_articles_info(url)
    if entities_sentiment == 'POS':
        data = []
        for entity_key in entities_dict.keys():
            if entities_dict[entity_key] > 0:
                data.append(entity_key)
        return create_radio_items_bootstrap(id = 'radios-article-timeline', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")

    if entities_sentiment == 'NEG':
        data = []
        for entity_key in entities_dict.keys():
            if entities_dict[entity_key] < 0:
                data.append(entity_key)
        return create_radio_items_bootstrap(id = 'radios-article-timeline', elements = [ {'label': entity, 'value': entity} for entity in data], labelClassName="date-group-labels_v3")
    return create_radio_items_bootstrap(id = 'radios-article-timeline', elements = [ {'label': entity, 'value': entity} for entity in entities_dict.keys()], labelClassName="date-group-labels_v3")

@app.callback(
    dash.dependencies.Output('entity-statistics', 'children'),
    [Input("radios-article-timeline", "value"), dash.dependencies.Input('article-slider', 'value')])
def show_entity_statistics(value, slider):
    timestamp_from_value = slider[0]
    timestamp_to_value = slider[1]
    articles = get_articles_from_to(timestamp_from_value, timestamp_to_value)
    positive = 0
    negative = 0
    neutral = 0
    for article in articles:
        _, entities = get_articles_info(article["url"])
        if value in entities:
            score = entities[value]
            if score > 0:
                positive += 1
            elif score < 0:
                negative += 1
            else:
                neutral += 1
    card_pos = create_card("Number of Positive", positive)
    card_neg = create_card("Number of Negative", negative)
    card_neu = create_card("Number of Neutral", neutral)
    return create_card_row([create_card_col(card_pos, width = 4), create_card_col(card_neg, width = 4), create_card_col(card_neu, width = 4)]),
# -----------------------------------------------------
#------------------------------------------------------

# GENERAL CALLBACKS
# callback from tabs
@app.callback(Output('tabs-content', 'children'),
              Input('tabs', 'value'))
def render_content(tab):
    if tab == 'tab-media-article':
        return tab_media_articles()
    elif tab == 'tab-topic-sentiment':
        return tab_topic_sentiment()
    elif tab == 'tab-time-line':
        return tab_time_line()
    elif tab == 'articles-tab':
        return tab_articles()
    elif tab == 'articles-tab-timeline':
        return tab_articles_timeline()
    elif tab == 'statistics-tab':
        return tab_statistics()
#------------------------------------------------------

# Run Application
if __name__ == '__main__':
    app.run_server(host='0.0.0.0')