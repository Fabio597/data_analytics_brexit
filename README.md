# Brexit - Data Analytics

## Installazione
Per eseguire il progetto è necessario aver installato sulla propria macchina **Docker** e **docker-compose**.

## Esecuzione del codice
Una volta installato Docker, è possibile lanciare il software tramite il comando **docker-compose up** dalla root del progetto (dove compare il file *docker-compose.yml*).\
Eseguendo in questo modo il programma verrà lanciato utilizzando i dati già processati (5280 link) che si trovano nella cartella *./mongo-data/*. Non appena le immaginni Docker e i container saranno pronti, sarà possibile utilizzare la dashboard per visualizzare i risultati.\
Se, invece, si desidera lanciare il programma da zero (impiegando più tempo) è necessario svolgere i seguenti passi:
- Svuotare la cartella *mongo-data*
- Svuotare le cartelle *ASUM_files_input* e *ASUM_files_output*
- popolare il file */dataset/import_dataset.csv* con i dati desiderati, purchè rispetti il formato tutt'ora presente: ***link date***
- Scommentare il file *main.py*

Una volta eseguite queste operazioni è necessario lanciare il comando **docker-compose up** dalla root del progetto.

## Visualizzazione della Dashboard
Una volta che il container **brexit_dashboard** sarà pronto, per la visualizzazione dell'interfaccia interattiva basterà andare al link: [http://127.0.0.1:8050/](http://127.0.0.1:8050/)

## Lavoro svolto da
Fabio D'Adda matricola: 817279