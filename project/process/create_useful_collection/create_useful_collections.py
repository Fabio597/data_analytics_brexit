from project.database.database import get_distinct_authors, get_media_entities, save_media_entities, get_distinct_dates, get_date_entities, save_timeline_entities
from project.process.text_preprocess_and_entities.clean_text.text_cleaner import TextCleaner

def create_media_entities_collections():
    media_articles_list = get_distinct_authors()
    for media in media_articles_list:
        entities = {}
        entities_list = []
        for article in list(get_media_entities(media)):
            if article['ner'] is not None:
                for entity in article['ner']:
                    if entity['word'].lower() in entities:
                        current_sentiment = entities[entity['word'].lower()]['sentiment']
                        new_sentiment = {'sentiment': current_sentiment + entity['sentiment']}
                        entities[entity['word'].lower()].update(new_sentiment)
                    else:
                        entities[entity['word'].lower()] = {'concept': entity['concept'], 'sentiment': entity['sentiment']}
                        entities_list.append(entity['word'].lower())
            if article['nel'] is not None:
                for entity in article['nel']:
                    if entity['word'].lower() in entities:
                        normalized_text = None
                        if entity['text'] is not None:
                            (normalized_text, _) = TextCleaner(entity['text']).normalize()
                        entities_list.append(entity['word'].lower())
                        entities[entity['word'].lower()].update(
                            {
                                'text': normalized_text, 
                                'link': entity['link'], 
                                'image': entity['image']
                            }
                        )

        save_media_entities(media, entities, entities_list)

def create_time_line_entities_collections():
    dates = get_distinct_dates()
    for date in dates:
        entities = {}
        entities_list = []
        for article in list(get_date_entities(date)):
            if article['ner'] is not None:
                for entity in article['ner']:
                    if entity['word'].lower() in entities:
                        current_sentiment = entities[entity['word'].lower()]['sentiment']
                        new_sentiment = {'sentiment': current_sentiment + entity['sentiment']}
                        entities[entity['word'].lower()].update(new_sentiment)
                    else:
                        entities[entity['word'].lower()] = {'concept': entity['concept'], 'sentiment': entity['sentiment']}
                        entities_list.append(entity['word'].lower())
            if article['nel'] is not None:
                for entity in article['nel']:
                    if entity['word'].lower() in entities:
                        normalized_text = None
                        if entity['text'] is not None:
                            (normalized_text, _) = TextCleaner(entity['text']).normalize()
                        entities_list.append(entity['word'].lower())
                        entities[entity['word'].lower()].update(
                            {
                                'text': normalized_text, 
                                'link': entity['link'], 
                                'image': entity['image']
                            }
                        )
        save_timeline_entities(date, entities, entities_list)

def create_collections_for_dashboard():
    create_media_entities_collections()
    create_time_line_entities_collections()

