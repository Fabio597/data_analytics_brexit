from nltk.stem import PorterStemmer
from project.database.database import get_articles_words, save_wordlist, save_wordstemming
from project.process.text_preprocess_and_entities.clean_text.regex import check_if_NOT_is_present, replace_NOT_word

# Porter Stemmer object
ps = PorterStemmer()
# create list of words to print on a file and save in MongoDB
# Return a set of words of all documents (without duplicated words)
def create_word_list():
    wordlist = set()
    words = get_articles_words()
    for current_word_list in list(words):
        current_words = set(current_word_list["words"])
        wordlist = wordlist.union(current_words)
    return wordlist

# Return list of words ordered alphabetically
def create_dict_word_position():
    words_position = {}
    words_stemming = {}
    words = sorted(list(create_word_list()))
    stemmed_words = []
    for index, word in enumerate(words):
        is_not_present = check_if_NOT_is_present(word)
        if is_not_present:
            current_stemming = ps.stem(replace_NOT_word(word))
            words_position[word] = index
            words_stemming[word] = current_stemming
            stemmed_words.append(current_stemming)
        else:
            current_stemming = ps.stem(word)
            words_position[word] = index
            words_stemming[word] = current_stemming
            stemmed_words.append(current_stemming)
    save_wordlist(words_position)
    save_wordstemming(words_stemming)
    return stemmed_words

# open -> begin from 'Progetto Data Aalytics' folder
def create_wordlist_file():
    words = create_dict_word_position()
    outF = open("project/process/ASUM_files_input/WordList.txt", "w")
    for word in words:
    # write line to output file
        outF.write(word)
        outF.write("\n")
    outF.close()