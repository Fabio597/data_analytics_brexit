from project.process.create_asum_files.create_wordlist import create_wordlist_file
from project.process.create_asum_files.create_bos import create_bos_file
from project.process.create_asum_files.create_setiwords_files import create_sentiwords_files

def create_asum_files():
    create_wordlist_file()
    create_bos_file()
    create_sentiwords_files()