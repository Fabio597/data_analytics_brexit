from project.database.database import get_articles_sentences, get_words_position

def create_bos_file():
    outF = open("project/process/ASUM_files_input/BagOfSentences.txt", "a")
    documents = get_articles_sentences()
    for document in documents:
        doc = document["sentences"]
        write_article_on_file(outF, str(len(doc)), doc)
    outF.close()

def covert_words_in_numbers(sentence):
    sentence_coded = []
    words_position = get_words_position()
    for word in sentence:
        sentence_coded.append(words_position[word])
    return sentence_coded

def write_article_on_file(output_file, number_of_sentences, sentences):
    output_file.write(number_of_sentences)
    output_file.write("\n")
    for sentence in sentences:
        sentence_coded = covert_words_in_numbers(sentence)
        for word_coded in sentence_coded:
            output_file.write(str(word_coded))
            output_file.write(" ")
        output_file.write("\n")