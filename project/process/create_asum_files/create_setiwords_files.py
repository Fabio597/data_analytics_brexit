from afinn import Afinn
from project.process.create_asum_files.create_wordlist import create_word_list
from project.database.database import get_words_stemming
from project.process.text_preprocess_and_entities.clean_text.regex import check_if_NOT_is_present, remove_NOT_word

# threshold for positive and negative sentiments
threshold_0 = 2
threshold_1 = -2

def create_sentiwords_lists():
    afinn = Afinn()
    sentiwords_0 = []
    sentiwords_1 = []
    wordlist = create_word_list()
    for word in wordlist:
        is_NOT_present = check_if_NOT_is_present(word)
        word_cleaned = remove_NOT_word(word)
        score = afinn.score(word_cleaned)
        if is_NOT_present:
            if score >= threshold_0:
                sentiwords_1.append("NOT_"+word_cleaned)
            elif score <= threshold_1:
                sentiwords_0.append("NOT_"+word_cleaned)
        else:
            if score >= threshold_0:
                sentiwords_0.append(word_cleaned)
            elif score <= threshold_1:
                sentiwords_1.append(word_cleaned)
    return (sentiwords_0, sentiwords_1)

def create_sentiwords_files():
    words_stemming = get_words_stemming()
    sentiwords_0, sentiwords_1 = create_sentiwords_lists()
    create_sentiwords_file_0(words_stemming, sentiwords_0)
    create_sentiwords_file_1(words_stemming, sentiwords_1)

def create_sentiwords_file_0(words_stemming, sentiwords_0):
    outF = open("project/process/ASUM_files_input/SentiWords-0.txt", "w")
    for word in sentiwords_0:
    # write line to output file
        outF.write(words_stemming[word])
        outF.write("\n")
    outF.close()

def create_sentiwords_file_1(words_stemming, sentiwords_1):
    outF = open("project/process/ASUM_files_input/SentiWords-1.txt", "w")
    for word in sentiwords_1:
    # write line to output file
        outF.write(words_stemming[word])
        outF.write("\n")
    outF.close()