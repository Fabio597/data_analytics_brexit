import re

def remove_numbers(text):
    return re.sub("\d+", "", text)

def remove_non_word_character(word):
    return re.sub("\W", "", word)

def replace_NOT_word(word):
    return re.sub("NOT_", "not", word)

def remove_NOT_word(word):
    return re.sub("NOT_", "", word)

def check_if_NOT_is_present(word):
    matched = re.match("NOT_", word)
    return bool(matched)

def replace_strange_characters(word):
    new_text = re.sub("ɛ", "e", word)
    new_text = re.sub("ɪ", "i", new_text)
    new_text = re.sub("ə", "o", new_text)
    new_text = re.sub("⅞", "", new_text)
    return new_text