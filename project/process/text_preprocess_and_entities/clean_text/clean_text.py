import nltk
from project.process.text_preprocess_and_entities.clean_text.stop_punct_rem import stop, punctuation
from project.process.text_preprocess_and_entities.clean_text.regex import remove_numbers, replace_strange_characters, remove_non_word_character

# test it to see if 'NOT_' is put in front of the word
def remove_stopwords_and_punctuation(sentence):
    cleaned_text = []
    negation_found = False
    for word in sentence:
        if word.lower() in ['not', 'don\'t', 'doesn\'t', 'nor', 'against']:
            negation_found = True
        if word.lower() in punctuation:
            negation_found = False
        if word.lower() not in stop and word.lower() not in punctuation and word.lower():
            if negation_found:
                cleaned_text.append(remove_non_word_character("NOT_"+word.lower()))
            else:
                cleaned_text.append(remove_non_word_character(word.lower()))
    return cleaned_text

# split sentences in words
def clean_text(sentence):
    list_of_words = nltk.word_tokenize(sentence)
    return remove_stopwords_and_punctuation(list_of_words)

# split text in sentences with nltk
def split_text_in_sentences(text):
    return nltk.tokenize.sent_tokenize(text)

# split and clean sentences
def apply_cleaning(text):
    cleaned_sentences = []
    splitted_text = split_text_in_sentences(text)
    for sentence in splitted_text:
        sentence_cleaned = clean_text(clean_with_regex(sentence))
        cleaned_sentences.append(sentence_cleaned)
    return cleaned_sentences

# functions to clean text
def clean_with_regex(text):
    step_1 = remove_numbers(text)
    step_2 = replace_strange_characters(step_1)
    return step_2