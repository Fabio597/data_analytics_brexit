import re

#Data Preparation class
class TextCleaner:
    class Pattern:
        def __init__(self, pattern, output):
            self.pattern = pattern
            self.output = output
        #matching patterns using regular expression
        def match(self, text):

            pattern = re.compile(self.pattern)
            matches = pattern.finditer(text)
            boundaries = []

            for match in matches:
                boundaries.append(match.span())

            (result, n) = re.subn(self.pattern, self.output, text)
            return (result, n, boundaries)
    
    norm_functions = [
        (Pattern(r'–', r'-'),                   "translated non standard -"),
        (Pattern(r'—', r'-'),                   "translated non standard -"),
        (Pattern(r'(\w|\W)-\s', r'\1 '),        "removed trailing -"),
        (Pattern(r'[\"\/]', r' '),              "removed bad characters"),
        (Pattern(r'\([^)]*\)|\[[^\]]*\]', r''), "removed round and square brackets"),
        (Pattern(r'\s{2,}', r' '),              "removed double spaces"),
        (Pattern(r'\s+-\s+', r', '),            "translated - to ,"),
        (Pattern(r'(\w)-(\w)', r'\1 \2'),       "removed - between two words"),
        (Pattern(r'\s+(,|\.)', r'\1'),          "removed spaces before commas and dots"),
    ]
    
    def __init__(self, raw_text):
        self.raw_text = raw_text
        self.applied_norm = []
        
    def normalize(self):
        result = self.raw_text

        for nFunc in TextCleaner.norm_functions:
            (result, applied, boundaries) = nFunc[0].match(result)
            if applied > 0:
                self.applied_norm.append((nFunc[1], applied, boundaries))
            
        return (result, self.applied_norm)
