from newspaper import Article
import re
import datetime
from project.process.import_data.import_data import read_brexit_words

def scraping(url, pub_date):
    brexit_words = read_brexit_words()
    current_article = Article(url)
    try:
        current_article.download()
        current_article.parse()
    except Exception as e:
        pass
    text = current_article.text
    text_approved = check_text(brexit_words, text)
    author = get_author(url)
    date = datetime.datetime.strptime(pub_date, "%Y-%m-%dT%H:%M:%S+00:00")
    # Date Normalization
    date = date.replace(hour=0, minute=0, second=0)
    if text is not None and text_approved:
        return (url, text, author, date)
    return (None, None, None, None)

def get_author(url):
    pattern = r'//(.*?)/'
    result = re.findall(pattern, url)
    if len(result) > 0:
        return result[0]
    else:
        return None

def check_text(brexit_words, text):
    for word in brexit_words:
        result = re.findall(word, text, re.IGNORECASE)
        if len(result) > 0:
            return True
    return False