from project.process.text_preprocess_and_entities.scraping.scraping import scraping
from project.process.text_preprocess_and_entities.clean_text.clean_text import apply_cleaning
from project.process.text_preprocess_and_entities.clean_text.text_cleaner import TextCleaner
from project.process.text_preprocess_and_entities.find_entities.NER.ner import find_media_entities
from project.process.text_preprocess_and_entities.find_entities.NEL.nel import entity_linking
from project.database.database import save_article, check_if_text_exist

from multiprocessing import Pool

# important function to clean text, clean sentences, clean words, 
# create sentences list, create words list and execute ner and nel
def text_preprocess_and_entities(articles):
    avoid_duplicated_url = set()
    for url, date in articles:
        if url not in avoid_duplicated_url:
            avoid_duplicated_url.add(url)
            (url, text, author, date) = scraping(url, date)
            if text is not None:
                # clean initial text
                (normalized_text, _) = TextCleaner(text).normalize()
                # create sentences cleaned
                sentences = apply_cleaning(normalized_text)
                # create words list
                words = create_words_list(sentences)
                # name entity recognition with spacy
                ner = find_media_entities(normalized_text)
                # name entity linking with DBpedia spotlight
                nel = entity_linking(normalized_text)
                # save data in Mongo
                save_article(url, normalized_text, author, date, sentences, words, ner, nel)

# as the function above but for a single process
def single_text_preprocess_and_entities(article):
    (url, text, author, date) = scraping(article[0], article[1])
    if text is not None:
        # clean initial text
        (normalized_text, _) = TextCleaner(text).normalize()
        if len(list(check_if_text_exist(normalized_text))) == 0:
            # create sentences cleaned
            sentences = apply_cleaning(normalized_text)
            # create words list
            words = create_words_list(sentences)
            # name entity recognition with spacy
            ner = find_media_entities(normalized_text)
            # name entity linking with DBpedia spotlight
            nel = entity_linking(normalized_text)
            # save data in Mongo
            save_article(url, normalized_text, author, date, sentences, words, ner, nel)
    

def create_words_list(sentences):
    words = []
    for sentence in sentences:
        words = words + sentence
    return words
    
