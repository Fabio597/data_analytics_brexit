import nltk
import en_core_web_sm
from afinn import Afinn
from project.process.text_preprocess_and_entities.clean_text.stop_punct_rem import punctuation
afinn = Afinn()
nlp = en_core_web_sm.load()

def find_media_entities(text):
    final_entities = []
    for sentence in nltk.tokenize.sent_tokenize(text):
        neg_words = check_negations(sentence)
        article_nlp = nlp(sentence)
        current_entities = [(art.text, art.label_) for art in article_nlp.ents]
        ner_article_entities = list(filter(filter_concepts, current_entities))
        for ent in ner_article_entities:
            sentiments = find_sentiment(article_nlp, ent[0], neg_words)
            final_entities.append({"word": ent[0], "concept": ent[1], "sentiment": int(sum(sentiments))})
    return final_entities
    
def check_negations(sentence):
    neg_words = []
    not_present = False
    for word in nltk.word_tokenize(sentence):
        if word.lower() in ['not', 'don\'t', 'doesn\'t', 'nor', 'against']:
            not_present = True
            continue
        if word.lower() in punctuation:
            not_present = False
        if not_present:
            neg_words.append(word.lower())
    return neg_words

# filter out concepts like: NUMBERS, DATES, ecc
def filter_concepts(tupla):
    if (tupla[1].upper() not in ['WORK_OF_ART', 'MONEY', 'DATE', 'TIME', 'PERCENT', 'QUANTITY', 'ORDINAL', 'CARDINAL']):
        return True
    else:
        return False

# Dependency Parsing
def build_dicts_for_dep_par(doc):
    dependencies = {}
    position_word = {}
    word_position = {}
    for token in doc:
        dependencies[token.i] = token.head.i
        word_position[token.text.lower()] = token.i
        position_word[token.i] = token.text.lower()
    return (dependencies, word_position, position_word)

def find_sentiment(doc, word, neg_words):
    sentiments = []
    (dependencies, word_position, position_word) = build_dicts_for_dep_par(doc)
    for token in nltk.word_tokenize(word):
        if token.lower() in word_position:
            searched_index = word_position[token.lower()]
            if searched_index in dependencies:
                sentiments.append(find_sentiment_from_index(
                    doc,
                    dependencies, 
                    position_word, 
                    dependencies[searched_index],
                    neg_words
                    )
                )
            else:
                sentiments.append(0)
        else:
            sentiments.append(0)
    return sentiments

def find_sentiment_from_index(doc, dependencies, position_word, index, neg_words):
    is_root = dependencies[index] == index
    word = position_word[index]
    score = afinn.score(word)
    # positive or negative
    if score != 0 and score != 1 and score != -1:
        if score > 0:
            # score positivo
            if word in neg_words:
                return -1
            else:
                return +1
        else:
            # score negativo
            if word in neg_words:
                return +1
            else:
                return -1
    else:
        if is_root:
            # neutral
            return 0
        else:
            # recursion
            return find_sentiment_from_index(doc, dependencies, position_word, dependencies[index], neg_words)