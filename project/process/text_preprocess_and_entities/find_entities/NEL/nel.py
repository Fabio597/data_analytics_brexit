import requests
from multiprocessing import Manager
from project.process.text_preprocess_and_entities.find_entities.NEL.SPARQL_Queries.sparql_queries import get_resource

manager = Manager()
nel_info = manager.dict()

def entity_linking(text):
    return execute_and_parse_response(text)

def make_request(text, confidence = 0.9):
    try:
        return requests.get(url="https://api.dbpedia-spotlight.org/en/annotate", params={"text": text, "confidence": confidence}, headers={"accept": "application/json"})
    except Exception as e:
        return None

def execute_and_parse_response(text):
    global nel_info
    words_and_link = []
    response = make_request(text)
    if response is not None:
        if response.status_code == 200:
            if 'Resources' in response.json():
                for element in response.json()['Resources']:
                    if element['@URI'] in nel_info:
                        words_and_link.append(nel_info[element['@URI']])
                    else:
                        text = get_resource(element['@URI'])
                        image = get_resource(element['@URI'], text = False)
                        current_info = {"word": element['@surfaceForm'], "link": element['@URI'], "text": text, "image": image}
                        nel_info[element['@URI']] = current_info
                        words_and_link.append(current_info)
    return words_and_link