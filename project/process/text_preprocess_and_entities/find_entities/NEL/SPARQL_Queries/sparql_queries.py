import SPARQLWrapper

def get_abstract_query(url_dbpedia):
    query = """
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        SELECT ?text
        WHERE {
           <"""+url_dbpedia+"""> dbo:abstract ?text .
            FILTER langMatches( lang(?text), 'EN' )
        }
    """
    return query

def get_image_query(url_dbpedia):
    query = """
        PREFIX foaf:  <http://xmlns.com/foaf/0.1/>
        SELECT ?image
        WHERE {
            <"""+url_dbpedia+"""> dbo:thumbnail ?image.
        }
    """
    return query

def execute_query(query):
    sparql = SPARQLWrapper.SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery(query)
    sparql.setReturnFormat(SPARQLWrapper.JSON)
    results = None
    try:
        results = sparql.query().convert()
    except Exception as e:
        print(e)
        print(query)
        return None
    
    return results

def parse_query(results, text = True):
    if(results is not None and len(results['results']['bindings'])!=0):
        res = results['results']['bindings'][0]
        if text:
            if 'text' in res and 'value' in res['text']:
                return res['text']['value']
            else:
                return None
        else:
            if 'image' in res and 'value' in res['image']:
                return res['image']['value']
            else:
                return None
    else:
        return None

def get_resource(url_dbpedia, text = True):
    if text:
        query = get_abstract_query(url_dbpedia)
        results = execute_query(query)
        return parse_query(results)
    else:
        query = get_image_query(url_dbpedia)
        results = execute_query(query)
        return parse_query(results, False)