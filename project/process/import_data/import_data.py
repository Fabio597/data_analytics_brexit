import os
import csv

FILE_PATH_TRAIN = "../../dataset/news_train.tsv"
FILE_PATH_TEST = "../../dataset/news_test.tsv"

def test_data_read():
    tsv_file_train = open(os.path.join(os.path.dirname(__file__), "../../dataset/import_dataset.tsv"))
    read_tsv_train = csv.reader(tsv_file_train, delimiter = "\t")
    return read_tsv_train

def read_train_data():
    tsv_file_train = open(os.path.join(os.path.dirname(__file__), FILE_PATH_TRAIN))
    read_tsv_train = csv.reader(tsv_file_train, delimiter = "\t")
    return read_tsv_train

def read_test_data():
    tsv_file_test = open(os.path.join(os.path.dirname(__file__), FILE_PATH_TEST))
    read_tsv_test = csv.reader(tsv_file_test, delimiter = "\t")
    return read_tsv_test

def read_brexit_words():
    brexit_words = []   
    with open(os.path.join(os.path.dirname(__file__), "brexit-words.txt")) as bwords:
        words = bwords.readlines()
    for word in words:
        brexit_words.append((word.rstrip()))
    return brexit_words