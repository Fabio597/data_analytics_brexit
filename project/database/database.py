import pymongo

# connect to mongo db with pymongo
def get_db():
  # Manually
  #client = pymongo.MongoClient("mongodb://localhost:27017/")
  # Docker
  client = pymongo.MongoClient("mongodb://mongo:27017/")
  db = client["brexitdb"]
  return db

def get_collection(collection = 'ARTICLES'):
  db = get_db()
  if collection == 'ARTICLES':
    return db["articles"]
  elif collection == 'WORDSLIST':
    return db["wordslist"]
  elif collection == 'WORDSSTEMMING':
    return db["wordsstemming"]
  elif collection == 'MEDIAENTITIES':
    return db['media_entities']
  elif collection == 'TIMELINEENTITIES':
    return db['time_line_entities']

articles = get_collection(collection = 'ARTICLES')
wordslist = get_collection(collection = 'WORDSLIST')
wordsstemming = get_collection(collection = 'WORDSSTEMMING')
media_entities = get_collection(collection = 'MEDIAENTITIES')
time_line_entities = get_collection(collection = 'TIMELINEENTITIES')


# SAVE FUCTIONS

# save article in MongoDB
def save_article(url, text, author, date, sentences = [], words = [], ner = [], nel = []):
  article = {"url": url, "text": text, "author": author, "date": date, "sentences": sentences, "words": words, "ner": ner, "nel": nel}
  inserted_articles = articles.insert_one(article)
  print(inserted_articles.inserted_id)

# save media entities
def save_media_entities(media, entities, entities_list):
  inserted_media_entities = media_entities.insert_one({"media": media, "entities": entities, "entities_list": entities_list})
  print(inserted_media_entities)

# save time line entities
def save_timeline_entities(date, entities, entities_list):
  inserted_date_entities = time_line_entities.insert_one({"date": date, "entities": entities, "entities_list": entities_list})
  print(inserted_date_entities)

# save word list
def save_wordlist(words_position):
  inserted_wordlist = wordslist.insert_one(words_position)
  print(inserted_wordlist.inserted_id)

# save word stemming
def save_wordstemming(words_stemming):
  inserted_wordstemming = wordsstemming.insert_one(words_stemming)
  print(inserted_wordstemming.inserted_id)

# RETRIEVE FUCTIONS

# number of articles: db.articles.count();
def get_number_of_articles():
  return articles.count()

# db.articles.find({}, {'sentences': 1}).pretty();
def get_articles_sentences():
  return articles.find({}, {'sentences': 1, '_id': 0})

# find by media (author): db.articles.find({"author": "nome autore"}, {"ner": 1, "nel": 1, "_id": 0});
def get_media_entities(media):
  return articles.find({"author": media}, {"ner": 1, "nel": 1, "_id": 0})

# find by date (date): db.articles.find({"date": "date"}, {"ner": 1, "nel": 1, "_id": 0});
def get_date_entities(date):
  return articles.find({"date": date}, {"ner": 1, "nel": 1, "_id": 0})

# get words position
def get_words_position():
  return wordslist.find_one()

# get words stemming
def get_words_stemming():
  return wordsstemming.find_one()

# get all media (distinct): db.articles.distinct("author");
def get_distinct_authors():
  return articles.find({}).distinct("author")

# get all dates (distinct): db.articles.distinct("date");
def get_distinct_dates():
  return articles.find({}).distinct("date")

# db.articles.find({}, {'words': 1}).pretty();
def get_articles_words():
  return articles.find({}, {'words': 1, '_id': 0})

def check_if_text_exist(text):
  return articles.find({"text": text},{'_id': 1})